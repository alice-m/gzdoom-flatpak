#!/bin/sh

APPID="org.zdoom.GZDoom"

flatpak-builder --repo=repo build $APPID.json --force-clean
flatpak build-bundle repo $APPID.flatpak $APPID